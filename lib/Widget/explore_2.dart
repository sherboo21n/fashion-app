import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/setting.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/shop.dart';
import 'package:fashionshop/Screans/women_collection.dart';
import 'package:flutter/material.dart';
import 'package:fashionshop/Models/explore2_model.dart';

class Explore2 extends StatefulWidget {
  @override
  _Explore2State createState() => _Explore2State();
}

class _Explore2State extends State<Explore2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height:500,
      child: ListView.builder(
        shrinkWrap: true,
          itemCount: explore2.length,
          itemBuilder: (context,index) =>InkWell(
            onTap: (){
             index == 1?  Navigator.push(context, MaterialPageRoute(builder: (context) =>WomenCollection())) :
             Navigator.push(context, MaterialPageRoute(builder: (context) =>Shop()));
            },
            child: Stack(children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Image.asset(explore2[index].image,fit: BoxFit.cover,
                  height: 300,width: 500,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 120,left: 110),
                child:  Column(
                  children: <Widget>[
                    Text(explore2[index].title,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 24),),
                    Text(explore2[index].desc,style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.w400))
                  ],
                ),
              )
            ],),
          )),
    );
  }
}
