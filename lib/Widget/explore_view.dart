import 'package:fashionshop/Widget/explore_2.dart';
import 'package:flutter/material.dart';


class Explore extends StatefulWidget {
  @override
  _ExploreState createState() => _ExploreState();
}

class _ExploreState extends State<Explore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: ListView(
       shrinkWrap: true,
       physics: ClampingScrollPhysics(),
       children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 150,top: 40),
          child:  Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Text('Explore',style: TextStyle(fontSize: 24),),
            IconButton(icon: Icon(Icons.shopping_cart,size: 23,),onPressed: (){},)
          ],),
        ),
         SizedBox(height: 15,),
       Container(
         margin: EdgeInsets.only(left: 15,right: 15),
         height: 40,
         decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
           color: Colors.grey.shade200
         ),
         child: TextField(
           decoration: InputDecoration(
               hintText: 'Search',
               suffixIcon: Icon(Icons.clear),
               prefixIcon: Icon(Icons.search),
               border: InputBorder.none
           ),
         ),
       ),
         SizedBox(height: 20,),
         Explore2(),
         SizedBox(height: 10,),


       ],
     ),
    );
  }
}
