import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';

class WomenCollectioCarsual extends StatefulWidget {
  @override
  _WomenCollectioCarsualState createState() => _WomenCollectioCarsualState();
}

class _WomenCollectioCarsualState extends State<WomenCollectioCarsual> {
  @override
  Widget build(BuildContext context) {
    return Container(height: 220,
      child: PageIndicatorContainer(
        align: IndicatorAlign.bottom,
        indicatorSpace:8,
        padding: EdgeInsets.all(5),
        indicatorColor: Colors.white,
        indicatorSelectorColor: Colors.black,
        shape: IndicatorShape.circle(size: 8),
        length:3,
        child: PageView(
          children: <Widget>[
            Image.asset('images/sale.png',fit: BoxFit.cover,),
            Image.asset('images/women_2.png',fit: BoxFit.cover,),
            Image.asset('images/women_1.png',fit: BoxFit.cover,)
          ],
        ),


      ),);
  }
}
