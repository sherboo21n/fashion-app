import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body:ListView(
        shrinkWrap: true,
        children: <Widget>[
        Container(
          height: 175,width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20,top: 50),
              child:
              Text('Setting',style: TextStyle(fontSize: 22),),
            ),
            SizedBox(height: 20,),
           Padding(
             padding: EdgeInsets.only(left: 10,right: 10),
             child:  Row(children: <Widget>[
               CircleAvatar(
                 backgroundImage: AssetImage('images/dan.png'),
                 radius: 35,
               ),
               SizedBox(width: 15,),
               Column(children: <Widget>[
                 Text('Steve Bowen',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                 Text('Gold Member',style: TextStyle(color: Colors.grey,fontSize: 16),),
               ],
               ),
               Spacer(),
               Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 25,)
             ],),
           )
          ],),
        ),
          SizedBox(height: 10,),
          Container(
              height: 120,
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(left: 15,top: 10,right: 10),
                child: Column(children: <Widget>[
                  Row(children: <Widget>[
                    Container(
                      height: 40,width: 40,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Icon(Icons.location_on,color: Colors.white,),
                    ),
                    SizedBox(width: 10,),
                    Text('Address',style: TextStyle(fontSize: 18,color: Colors.black),),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 30,)
                  ],),
                  Divider(color: Colors.grey,),
                  Row(children: <Widget>[
                    Container(
                      height: 40,width: 40,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Icon(Icons.check_circle,color: Colors.white,),
                    ),
                    SizedBox(width: 10,),
                    Text('Checkout',style: TextStyle(fontSize: 18,color: Colors.black),),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 25,)
                  ],),
                ],),
              )
          ),
          SizedBox(height: 10,),
          Container(
              height: MediaQuery.of(context).size.height/2,
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(left: 15,top: 10,right: 10),
                child: Column(children: <Widget>[
                  Row(children: <Widget>[
                    Container(
                      height: 40,width: 40,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Icon(Icons.notifications_active,color: Colors.white,),
                    ),
                    SizedBox(width: 10,),
                    Text('Notification',style: TextStyle(fontSize: 18,color: Colors.black),),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 30,)
                  ],),
                  Divider(color: Colors.grey,),
                  Row(children: <Widget>[
                    Container(
                      height: 40,width: 40,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Icon(Icons.description,color: Colors.white,),
                    ),
                    SizedBox(width: 10,),
                    Text('Terms & Privacy Policy',style: TextStyle(fontSize: 18,color: Colors.black),),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 30,)
                  ],),
                  Divider(color: Colors.grey,),
                  Row(children: <Widget>[
                    Container(
                      height: 40,width: 40,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Icon(Icons.help,color: Colors.white,),
                    ),
                    SizedBox(width: 10,),
                    Text('Contact us',style: TextStyle(fontSize: 18,color: Colors.black),),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios,color: Colors.grey.shade200,size: 25,)
                  ],),
                ],),
              )
          )
      ],)

    );
  }
}
