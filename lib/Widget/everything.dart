import 'package:fashionshop/Models/everything_model.dart';
import 'package:fashionshop/Screans/home_page.dart';
import 'package:flutter/material.dart';

class EveryThing extends StatefulWidget {
  @override
  _EveryThingState createState() => _EveryThingState();
}

class _EveryThingState extends State<EveryThing> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(itemCount: products.length,physics: ClampingScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context , index)=>  Stack(children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,height: MediaQuery.of(context).size.height,
          child: Padding(padding: const EdgeInsets.all(8.0),
            child: ClipRRect(borderRadius: BorderRadius.circular(15),
              child: GridTile(child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>HomePage()));
                },
                child: Image.asset( products[index].image,fit: BoxFit.cover,),
              ),
              ),),
          ),),
        Align(
          alignment: Alignment.bottomLeft,
          child:  Container(
              padding: EdgeInsets.only(left: 8,bottom: 10),
              height: 30,width: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white
              ),
              child: Center(child:
              Text('New',style: TextStyle(color: Colors.black),),)
          ),
        )

      ],),

      );

  }
}
