import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';

class CarsualPro extends StatefulWidget {
  @override
  _CarsualProState createState() => _CarsualProState();
}

class _CarsualProState extends State<CarsualPro> {
  @override
  Widget build(BuildContext context) {
    return Container(height: 220,
      child: PageIndicatorContainer(
        align: IndicatorAlign.bottom,
          indicatorSpace:8,
          padding: EdgeInsets.all(5),
          indicatorColor: Colors.white,
          indicatorSelectorColor: Colors.black,
          shape: IndicatorShape.circle(size: 8),
          length:4,
          child: PageView(
            children: <Widget>[
             Image.asset('images/women_1.png',fit: BoxFit.cover,),
              Image.asset('images/men_1.png',fit: BoxFit.cover,),
              Image.asset('images/women_2.png',fit: BoxFit.cover,),
              Image.asset('images/women_1.png',fit: BoxFit.cover,)
            ],
          ),


      ),);
  }
}
