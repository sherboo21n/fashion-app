import 'package:badges/badges.dart';
import 'package:fashionshop/Providers/order_provider.dart';
import 'package:fashionshop/Screans/bag.dart';
import 'package:fashionshop/Screans/on_boarding.dart';
import 'package:fashionshop/Widget/best_sell.dart';
import 'package:fashionshop/Widget/carsual_pro.dart';
import 'package:fashionshop/Widget/explore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Shop extends StatefulWidget {
  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<Shop> {
  @override
  Widget build(BuildContext context) {
    final order = Provider.of<Order>(context);
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
       Stack(children: <Widget>[
         CarsualPro(),
         Padding(
           padding: const EdgeInsets.only(top: 20,left: 360),
           child:  Badge(animationType: BadgeAnimationType.scale,
             badgeContent: Text(order.count <9 ? order.count.toString() :  "9+"),
             position: BadgePosition.topRight(top: 5,right: 5),
             child: IconButton(
               icon: Icon(Icons.shopping_cart,),
               onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>Bag()));},
             ),),
         ),
       ],),
          SizedBox(height: 15,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
              Text('Best Sell',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                Text('See all',style: TextStyle(color: Colors.grey.shade800,fontWeight: FontWeight.w600),)
            ],),
          ),
          SizedBox(height: 15,),
          BestSell(),
          Divider(color: Colors.grey,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Explore',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                Text('See all',style: TextStyle(color: Colors.grey.shade800,fontWeight: FontWeight.w600),)
              ],),
          ),
          SizedBox(height: 15,),
          Explore(),
          SizedBox(height: 15,),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Top Trends',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                Text('See all',style: TextStyle(color: Colors.grey.shade800,fontWeight: FontWeight.w600),)
              ],),
          ),
          SizedBox(height: 15,),
          BestSell(),
          SizedBox(height: 10,),
      ],),
    );
  }
}
