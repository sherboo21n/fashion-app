import 'package:fashionshop/Screans/home_page.dart';
import 'package:fashionshop/Screans/product_detailes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fashionshop/Models/Best_sell_model.dart';

class BestSell extends StatefulWidget {
  @override
  _BestSellState createState() => _BestSellState();
}

class _BestSellState extends State<BestSell> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 190,
      child: ListView.builder(itemCount: best.length,
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemBuilder: (context,index) =>Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>ProductDetails(
                  title: best[index].title,
                  desc1: best[index].desc1,
                  price: best[index].prics,
                  image: best[index].image,
                  desc2: best[index].desc2,
                )));
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 15,right: 15),
                child: Image.asset(best[index].image,fit: BoxFit.cover,
                height: 150,
                  width: 130,
                ),
              ),
            ),
            SizedBox(height: 5,),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text(best[index].title,style: TextStyle(fontWeight: FontWeight.bold),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text(best[index].prics.toString(),),
            ),


          ],)),
    );
  }
}
