import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/setting.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/shop.dart';
import 'package:fashionshop/Screans/women_collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fashionshop/Models/explore.dart_model';

class Explore extends StatefulWidget {
  @override
  _ExploreState createState() => _ExploreState();
}

class _ExploreState extends State<Explore> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      child: ListView.builder(
        itemCount: explore.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemBuilder: (context,index) =>InkWell(
            onTap: (){
              index == 2?  Navigator.push(context, MaterialPageRoute(builder: (context) =>WomenCollection())) :
              Navigator.push(context, MaterialPageRoute(builder: (context) =>Shop()));
            },
            child: Stack(children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Image.asset(explore[index].image,fit: BoxFit.cover,
                  height:125 ,width: MediaQuery.of(context).size.width,
                ),
              ),
              index.isEven ?  Padding(
                padding: const EdgeInsets.only(top: 40,left: 300),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(explore[index].title,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),),
                    Text(explore[index].desc,style: TextStyle(color: Colors.white,fontSize: 14)),
                  ],),
              ) :  Padding(
                padding: const EdgeInsets.only(top: 40,left: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(explore[index].title,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),),
                    Text(explore[index].desc,style: TextStyle(color: Colors.white,fontSize: 14)),
                  ],),
              )

            ],),
          )),
      
    );
  }
}
