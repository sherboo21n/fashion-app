import 'package:fashionshop/Models/category_model.dart';
import 'package:fashionshop/Widget/setting.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/everything.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/setting.dart';
import 'package:fashionshop/Widget/women_collection_carsual.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WomenCollection extends StatefulWidget {
  @override
  _WomenCollectionState createState() => _WomenCollectionState();
}

class _WomenCollectionState extends State<WomenCollection> {

  int selectedIndex = 0;

  final tabs  =[
    EveryThing(),
    EveryThing(),
    EveryThing(),
    EveryThing(),
    EveryThing(),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20,right: 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,),onPressed: (){
              Navigator.pop(context);
            },),
            Text('For Women',style: TextStyle(color: Colors.black,fontSize: 20),),
            IconButton(icon: Icon(Icons.shopping_cart,color: Colors.black,),onPressed: (){},),
          ],),
        ),
        SizedBox(height: 10,),
        WomenCollectioCarsual(),
        SizedBox(height: 15,),
       Padding(
        padding: EdgeInsets.only(right: 10,left: 15),
         child: Row(children: <Widget>[
           Column(children: <Widget>[
             Text('Women`s Clothing',style:TextStyle(fontWeight: FontWeight.bold,fontSize: 18)),
             Text('741 items found            ',style: TextStyle(color: Colors.grey,),)
           ],
           ),
           Spacer(),
           IconButton(icon: Icon(Icons.filter_list),onPressed: (){},)
         ],),
      ),
        SizedBox(height: 20,),
       Container(
         height: 35,
         child: ListView.builder(
           shrinkWrap: true,
           scrollDirection: Axis.horizontal,
           itemCount: categories.length,
           itemBuilder: (context,index) =>InkWell(
             onTap: (){
              setState(() {
                selectedIndex = index;
              });
             },
             child: Padding(
                 padding: EdgeInsets.only(left: 8,right: 10),
                 child: Container(
                     width: 100,
                     decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(20),
                         color: Colors.grey.shade300
                     ),
                     child: Center(child:
                      Text(categories[index].title,style: TextStyle(color: Colors.black)),)
                 )),
           ))),
       SizedBox(height: 10,),
       Container(
         height: 450,
         child: tabs[selectedIndex],
       )


      ],),
    );
  }
}
