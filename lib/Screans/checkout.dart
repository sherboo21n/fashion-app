import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckOut extends StatefulWidget {
  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {

  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Row(children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back_ios,),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
              SizedBox(width: 130,),
              Text("Checkout",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),)
            ],),
          ),

          SizedBox(height: 10,),
          Padding(
            padding: const EdgeInsets.only(left: 15,top: 15),
            child: Row(children: <Widget>[
              Icon(Icons.check_circle,color: Colors.black,),
              SizedBox(width: 5,),
              Icon(Icons.location_on),
              SizedBox(width: 5,),
             Text('-------------------------------'),
              SizedBox(width: 5,),
             _expanded ?  Icon(Icons.check_circle,color: Colors.black,) :
             CircleAvatar(
                backgroundColor: Colors.grey.shade500,
                radius: 10,
                child: Center(child: Text('2',style: TextStyle(color: Colors.white),)),
              ) ,
              SizedBox(width: 10,),
              Icon(Icons.credit_card),
              SizedBox(width: 5,),
              Text('-------------------------------'),
              CircleAvatar(
                backgroundColor: Colors.grey.shade500,
                radius: 10,
                child: Center(child: Text('3',style: TextStyle(color: Colors.white),)),
              ),
            ],),
          ),
          SizedBox(height: 15,),
          Divider(color: Colors.grey,),
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Text('Shipping',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 28),),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5,left: 15,right: 15),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                   labelText: 'Full Name',
                      labelStyle: TextStyle(color: Colors.grey)
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'Address 1',
                      labelStyle: TextStyle(color: Colors.grey)
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'Address 2',
                      labelStyle: TextStyle(color: Colors.grey)
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15,right: 100),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("City",style: TextStyle(color: Colors.grey),),
                          Text("Zip Code",style: TextStyle(color: Colors.grey))
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 100),
                        child: Divider(color: Colors.black,),
                      )
                    ],
                  )
                ),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'State',
                    labelStyle: TextStyle(color: Colors.grey)
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20,),
          Align(alignment: Alignment.bottomCenter,
              child: Padding(padding: const EdgeInsets.only(bottom:24,left: 16,right: 16),
                child: SizedBox(width: double.infinity,height: 50,
                  child: RaisedButton(shape: StadiumBorder(),
                      child: Text("Continue to Payment",style: TextStyle(color: Colors.white ,fontSize: 16,letterSpacing: 1)),
                      color: Colors.black, onPressed:(){
                    setState(() {
                      _expanded = !_expanded;
                    });
                      }),),))
        ],
      ),
    );
  }
}
