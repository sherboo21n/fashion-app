import 'package:flutter/material.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:fashionshop/Models/page_model.dart';

import 'home_page.dart';
import 'login.dart';

class OnBoordingScrean extends StatefulWidget {
  @override
  _OnBoordingScreanState createState() => _OnBoordingScreanState();
}

class _OnBoordingScreanState extends State<OnBoordingScrean> {


  ValueNotifier<int> _pageviewnotifier = ValueNotifier(0);


  @override
  Widget build(BuildContext context) {
   pages;
    return Stack(children: <Widget>[
      Scaffold(
        body: PageView.builder(itemBuilder: (context , index){
          return Stack(children: <Widget>[
            Center(
              child: Container(
                height:350,width: 200,
                decoration: BoxDecoration(
                image: DecorationImage(image: ExactAssetImage(
                    pages[index].image), fit: BoxFit.cover),),),
            ),

            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:50),
                  child: Text(pages[index].title, style: TextStyle(color: Colors.black, fontSize: 24,fontWeight: FontWeight.w600)),
                ),
                Padding(padding: const EdgeInsets.only(left: 48,right: 48,top: 8),
                  child: Text(pages[index].descrebtion,
                    style: TextStyle(color: Colors.black,fontSize: 14),textAlign: TextAlign.center,),),
              ],),
            index == 2 ? Align(alignment: Alignment.bottomCenter,
                child: Padding(padding: const EdgeInsets.only(bottom:24,left: 16,right: 16),
                  child: SizedBox(width: double.infinity,height: 50,
                    child: RaisedButton(shape: StadiumBorder(),
                        child: Text("Start Shopping",style: TextStyle(color: Colors.white ,fontSize: 16,letterSpacing: 1)),
                        color: Colors.black, onPressed:(){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>Login()));
                        }),),)) : Text("")
          ],);

        },
          itemCount: pages.length,
          onPageChanged: (index){
            _pageviewnotifier.value = index;
          },

        ),
      ),
      Transform.translate(offset: Offset(0, 250),
        child: Align(
          alignment: Alignment.center,
          child: _displayPageIndicators(pages.length), ),),

    ],);


  }
  Widget _displayPageIndicators(int length){
    return Padding(
      padding: const EdgeInsets.only(bottom: 120),
      child: PageViewIndicator(
        pageIndexNotifier: _pageviewnotifier,
        length: length,
        normalBuilder: (animationController, index) => Circle(
          size: 6,
          color: Colors.grey,
        ),
        highlightedBuilder: (animationController, index) => ScaleTransition(
          scale: CurvedAnimation( parent: animationController, curve: Curves.ease,),
          child: Circle( size: 12.0, color: Colors.black),
        ),
      ),
    );

  }
}





