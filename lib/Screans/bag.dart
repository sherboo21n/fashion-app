import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fashionshop/Providers/product_provider.dart';
import 'package:fashionshop/Providers/order_provider.dart';
import 'package:provider/provider.dart';

import 'checkout.dart';

class Bag extends StatefulWidget {
  @override
  _BagState createState() => _BagState();
}

class _BagState extends State<Bag> {
  @override
  Widget build(BuildContext context) {
    final order = Provider.of<Order>(context);
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Row(children: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back_ios,),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            SizedBox(width: 130,),
            Text("My Bag",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),)
          ],),
        ),

          Container(
            height: 300,
            child: ListView.builder(itemCount: order.order.length,
                itemBuilder: (context , index) => Padding(
                  padding: const EdgeInsets.all(12),
                  child: Row(children: <Widget>[
                   Container(
                     height: 70,width: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      image: DecorationImage(
                        image: ExactAssetImage(
                          order.order[index].image
                        ),
                        fit: BoxFit.cover,
                      )
                    ),
                  ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(order.order[index].title,style: TextStyle(fontWeight: FontWeight.bold),),
                          SizedBox(height: 2,),
                          Text(order.order[index].prics.toString(),style: TextStyle(fontWeight: FontWeight.w500),),
                         SizedBox(height: 5,),
                         Row(children: <Widget>[
                           Text('Size; M',style: TextStyle(color: Colors.grey),),
                           SizedBox(width: 10,),
                           Text('Color; Grey',style: TextStyle(color: Colors.grey),)
                         ],)
                        ],),
                    )
                  ],),
                )),
          ),

          Divider(color: Colors.grey,),
          SizedBox(height: 20,),
          Padding(
            padding: EdgeInsets.only(left: 8,right: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Total amount',),
                Text(order.totalPrice.toString(),style: TextStyle(fontWeight: FontWeight.w500,color: Colors.black),)
              ],
            ),
          ),
          SizedBox(height: 30,),
          Align(alignment: Alignment.bottomCenter,
              child: Padding(padding: const EdgeInsets.only(bottom:24,left: 16,right: 16),
                child: SizedBox(width: double.infinity,height: 50,
                  child: RaisedButton(shape: StadiumBorder(),
                      child: Text("Checkout",style: TextStyle(color: Colors.white ,fontSize: 16,letterSpacing: 1)),
                      color: Colors.black, onPressed:(){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>CheckOut()));

                      }),),))
        ],
      )
    );
  }
}

