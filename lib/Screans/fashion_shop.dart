import 'package:fashionshop/Screans/signup.dart';
import 'package:flutter/material.dart';
import 'login.dart';

class FashionScrean extends StatefulWidget {
  @override
  _FashionScreanState createState() => _FashionScreanState();
}

class _FashionScreanState extends State<FashionScrean> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Image.asset('images/model.png',fit: BoxFit.cover,
        height: MediaQuery.of(context).size.height,
        ),
        Center(
          child:  Text('Fashion Shop',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 32),),
        ),
        Center(
          child:  Padding(
            padding: const EdgeInsets.only(top: 50,),
            child: Text('Best Fashion Shopping App',style: TextStyle(color: Colors.white,fontSize: 12),),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 550,right: 30,left: 40),
          child: Column(children: <Widget>[
            RaisedButton(
              shape: StadiumBorder(),
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(left: 125,right: 125,top: 15,bottom: 15),
                child: Text('Sign Up',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),),
              ),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>Login()));
              },),
            SizedBox(height: 15,),
            RaisedButton(
              shape: StadiumBorder(),
              color: Colors.black,
               child: Padding(
                padding: const EdgeInsets.only(left: 70,right: 70,top: 17,bottom: 17),
                child: Text('Continue with Facebook',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white),),
              ),
              onPressed: (){},),
          ],)
        )


      ],),
    );
  }
}
