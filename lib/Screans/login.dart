import 'package:fashionshop/Screans/home_page.dart';
import 'package:fashionshop/Screans/signup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'on_boarding.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  final GlobalKey<FormState> _formKey = GlobalKey();
  bool _isLoading = false;

  final _focusPasswowdNode = FocusNode();
  final _focusEmailNode = FocusNode();


  @override
  void dispose() {
    _focusPasswowdNode.dispose();
    _focusEmailNode.dispose();
    super.dispose();
  }

  Future<void> sumbit() async{
    final _formData = _formKey.currentState;
    if(_formData.validate()){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>HomePage()));
    }
    _formKey.currentState.save();
//    setState(() {
//      _isLoading = true;
//    });
//    try{
//      if(_authMode == AuthMode.Login){
//        await Provider.of<Auth>(context , listen: false).login(_authData["email"], _authData["password"]);
//      }else {
//        await  Provider.of<Auth>(context , listen: false).signup(_authData["email"], _authData["password"]);
//      }

    }
//    on HttpException catch(error){
//      var errorMessage = "Authentication failed!";
//      if(errorMessage.toString().contains("EMAIL_EXISTS")){
//        errorMessage = "The e-mail address is already use";
//      } else if(error.toString().contains("INVALID_EMAIL")){
//        errorMessage = "This is not a vaild e-mail address";
//      } else if(error.toString().contains("WEAK_PASSWORD")){
//        errorMessage = "The password is too weak";
//      } else if(error.toString().contains("EMAIL_NOT_FOUND")){
//        errorMessage = "Coudnt found a user with this e-mail";
//      } else if(error.toString().contains("INVALID_PASSWORD")){
//        errorMessage = "Invaild Passwrd";
//      }
//      showeDieloge(errorMessage);
//    }
//    catch(eroor){
//      const errorMessage = "Coudnt authenticate you can try again later";
//      showeDieloge(errorMessage);
//    }
//    setState(() {
//      _isLoading  =false;
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 70,left: 10),
          child: Text('Login',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 28),),
        ),
          SizedBox(height: 20,),
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: <Widget>[
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  onFieldSubmitted: (_){
                    FocusScope.of(context).requestFocus(_focusPasswowdNode);},
                  decoration: InputDecoration(
                    hintText: 'E-Mail',
                  ),
                  validator: (value){
                    if(value.isEmpty || !value.contains("@")){
                      return "Invaild Email";
                    }return null;
                  },
                  onSaved: (value){},
                ),
                SizedBox(height: 30,),
                TextFormField(
                  focusNode: _focusPasswowdNode,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    suffixIcon: Icon(Icons.remove_red_eye)
                  ),
                  validator: (value){
                    if(value.isEmpty || value.length < 5){
                      return "Your Password Must Be > 5";
                    }return null;
                  },
                  onSaved: (value){},

                )
              ],),
            )
          ),
        SizedBox(height: 25,),
        Padding(
          padding: const EdgeInsets.only(left: 30,right: 30),
          child: RaisedButton(
            shape: StadiumBorder(),
            color: Colors.black,
            child: Padding(
              padding: const EdgeInsets.only(left: 125,right: 125,top: 15,bottom: 15),
              child: Text('Log In',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white),),
            ),
            onPressed: sumbit),
        ),
        SizedBox(height: 15,),
        Center(child: Row(mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          Text('------------------------------  ',
            style: TextStyle(fontSize: 18,color: Colors.grey),),
          Text('OR ',
            style: TextStyle(fontSize: 18,color: Colors.grey.shade900),),
          Text(' ------------------------------',
            style: TextStyle(fontSize: 18,color: Colors.grey),)
        ],)),
        SizedBox(height: 15,),
       Center(
         child: Row(mainAxisAlignment: MainAxisAlignment.center,
           children: <Widget>[
           Container(
             height: 35,width: 35,
             decoration: BoxDecoration(
                 shape: BoxShape.circle,
                 border: Border.all(
                   color: Colors.grey
                 )
             ),
             child: Center(child: Text('f',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18))),
           ),
             SizedBox(width: 15,),
             Container(
             height: 35,width: 35,
             decoration: BoxDecoration(
                 shape: BoxShape.circle,
                 border: Border.all(
                     color: Colors.grey
                 )
             ),
               child: Center(child: Text('G+',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18),)),
           ),
       ],),),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Dont have an account ?',style: TextStyle(color: Colors.grey),),
            FlatButton(
              child: Text('Sign Up',style: TextStyle(fontWeight: FontWeight.w400)),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>SignUp()));
              },
            )
          ],
        ),
        Center(
            child:  Text('Forget Password',
                style: TextStyle(fontWeight: FontWeight.w400)),
        )


      ],),
    );
  }
}
