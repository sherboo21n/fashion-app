import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/setting.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/fashion_shop/lib/Widget/shop.dart';
import 'package:flutter/material.dart';

import '../Widget/explore_view.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  void _onChange(int val){
    setState(() {
      _currentIndex = val;
    });
  }

  final tabs = [
   Shop(),
  Explore(),
    Shop(),
    Setting(),
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        bottomNavigationBar:  BottomNavigationBar(

            type: BottomNavigationBarType.fixed,
            iconSize: 25,
            selectedItemColor: Colors.black,
            currentIndex: _currentIndex,
            onTap: _onChange,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text("Shop",style: TextStyle(fontSize: 15),),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.track_changes),
                title: Text("Explore",style: TextStyle(fontSize: 15)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.search),
                title: Text("Search",style: TextStyle(fontSize: 15)),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text("Setting",style: TextStyle(fontSize: 15)),
              ),
            ],
          ),
        body: tabs[_currentIndex]


    );
  }


}

