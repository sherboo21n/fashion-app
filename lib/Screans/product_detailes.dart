import 'package:badges/badges.dart';
import 'package:fashionshop/Models/conatiner_size_model.dart';
import 'package:fashionshop/Models/container_colors_model.dart';
import 'package:fashionshop/Models/everything_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:fashionshop/Providers/product_provider.dart';
import 'package:fashionshop/Providers/order_provider.dart';

import 'bag.dart';


class ProductDetails extends StatefulWidget {

  final String title , desc1 ,desc2, image;
  final double price;
  ProductDetails({this.price,this.title,this.desc1,this.image,this.desc2});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {


  int selectedIndex = 0;
  int selectedIndex2 = 0;
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    final order = Provider.of<Order>(context);
    final product = Provider.of<Product>(context);
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
       Stack(children: <Widget>[
         Image.asset(widget.image,fit: BoxFit.cover,
         height: 250,
         width: MediaQuery.of(context).size.width,),
         Padding(
           padding: const EdgeInsets.only(top: 15),
           child: Row(children: <Widget>[
             IconButton(icon:Icon (Icons.arrow_back_ios),onPressed: (){
               Navigator.pop(context);
             },),
             Spacer(),
             IconButton(icon:Icon (Icons.search),onPressed: (){},),
             Badge(animationType: BadgeAnimationType.scale,
               badgeContent: Text(order.count <9 ? order.count.toString() :  "9+"),
               position: BadgePosition.topRight(top: 5,right: 5),
               child: IconButton(
                 icon: Icon(Icons.shopping_cart,),
                 onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=>Bag()));},
               ),),
           ],),
         )
       ],),
        Container(
          height: MediaQuery.of(context).size.height/2,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 15,right: 15),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(widget.title,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Column(children: <Widget>[
                      Text('\$39.99',style: TextStyle(color: Colors.grey),),
                      Text('  \$${widget.price.toString()}',
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),textDirection: TextDirection.rtl,),
                    ],),
                  )
                ],),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Row(mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.star,color: Colors.yellow.shade700,),
                  Icon(Icons.star,color: Colors.yellow.shade700,),
                  Icon(Icons.star,color: Colors.yellow.shade700,),
                  Icon(Icons.star,color: Colors.grey.shade300,),
                  Icon(Icons.star,color: Colors.grey.shade300,),
                  SizedBox(width: 5,),
                  Text('15 reviews',style: TextStyle(color: Colors.black),)
                ],
              ),
            ),
            SizedBox(height: 7,),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text("Colors:",style: TextStyle(color: Colors.grey),),
            ),
            Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Container(
                  height: 50,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: cccc.length,
                    itemBuilder: (context , index) =>GestureDetector(
                        onTap: (){
                          setState(() {
                            selectedIndex = index;
                          });
                        },
                        child:  Row(children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color:  index == selectedIndex ? Colors.black : Colors.transparent,
                              ),
                            ),
                            child: cccc[index].container,
                          )
                        ],)
                    ),),
                )
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text("Size:",style: TextStyle(color: Colors.grey),),
            ),
            Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Container(
                  height: 50,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: ssss.length,
                    itemBuilder: (context , index) =>GestureDetector(
                        onTap: (){
                          setState(() {
                            selectedIndex2 = index;
                          });
                        },
                        child:  Row(children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color:  index == selectedIndex2 ? Colors.black : Colors.transparent,
                              ),
                            ),
                            child: ssss[index].container2,
                          )
                        ],)
                    ),),
                )
            ),
            SizedBox(height: 5,),
            Padding(
              padding: EdgeInsets.only(left: 10,right: 10),
              child: Text(widget.desc1),
            ),
            if(_expanded)
              Padding(
                padding: const EdgeInsets.only(left: 10,right: 10),
                child: Text(widget.desc2),
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text('View More',style: TextStyle(fontWeight: FontWeight.bold),),
                IconButton(icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
                  onPressed: (){
                    setState(() {
                      _expanded = !_expanded;
                    });
                  },)
              ],
            ),
            Divider(color: Colors.grey,),
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text('Shop the look',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
            ),
            Container(
              height: 120,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: products.length,
                  itemBuilder: (context,index) =>Padding(
                    padding: const EdgeInsets.all(12),
                    child: Row(children: <Widget>[
                      Container(
                        width: 70,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            image: DecorationImage(
                                image: AssetImage(products[index].image),
                                fit: BoxFit.cover
                            )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(products[index].title,style: TextStyle(fontWeight: FontWeight.bold),),
                            SizedBox(height: 2,),
                            Text(products[index].price.toString(),style: TextStyle(fontWeight: FontWeight.w500),)
                          ],),
                      )
                    ],),
                  )),
            ),
              Divider(color: Colors.grey,),
            ],),
        ),
          Container(
            height: 100,
            child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context,index) =>
                    Align(alignment: Alignment.bottomCenter,
                        child: Padding(padding: const EdgeInsets.only(bottom:24,left: 16,right: 16),
                          child: SizedBox(width: double.infinity,height: 50,
                            child: RaisedButton(shape: StadiumBorder(),
                                child: Text("Add to Bag",style: TextStyle(color: Colors.white ,fontSize: 16,letterSpacing: 1)),
                                color: Colors.black, onPressed:(){
                                  order.add(product.product[index]);
                                }),),))),
          )
      ],),
    );
  }
}
