import 'package:fashionshop/Screans/home_page.dart';
import 'package:fashionshop/Screans/login.dart';
import 'package:flutter/material.dart';

import 'on_boarding.dart';
import 'on_boarding.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  bool _isLoading = false;

  final _focusPasswowdNode = FocusNode();
  final _focusEmailNode = FocusNode();


  @override
  void dispose() {
    _focusPasswowdNode.dispose();
    _focusEmailNode.dispose();
    super.dispose();
  }


  Future<void> sumbit() async{
    final _formData = _formKey.currentState;
    if(_formData.validate()){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>HomePage()));
    }
    _formKey.currentState.save();
//    setState(() {
//      _isLoading = true;
//    });
//    try{

//      if(_authMode == AuthMode.Login){
//        await Provider.of<Auth>(context , listen: false).login(_authData["email"], _authData["password"]);
//      }else {
//        await  Provider.of<Auth>(context , listen: false).signup(_authData["email"], _authData["password"]);
//      }

    }
//    on HttpException catch(error){
//      var errorMessage = "Authentication failed!";
//      if(errorMessage.toString().contains("EMAIL_EXISTS")){
//        errorMessage = "The e-mail address is already use";
//      } else if(error.toString().contains("INVALID_EMAIL")){
//        errorMessage = "This is not a vaild e-mail address";
//      } else if(error.toString().contains("WEAK_PASSWORD")){
//        errorMessage = "The password is too weak";
//      } else if(error.toString().contains("EMAIL_NOT_FOUND")){
//        errorMessage = "Coudnt found a user with this e-mail";
//      } else if(error.toString().contains("INVALID_PASSWORD")){
//        errorMessage = "Invaild Passwrd";
//      }
//      showeDieloge(errorMessage);
//    }
//    catch(eroor){
//      const errorMessage = "Coudnt authenticate you can try again later";
//      showeDieloge(errorMessage);
//    }
//    setState(() {
//      _isLoading  =false;
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 70,left: 10),
          child: Text('SignUp',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 28),),
        ),
        SizedBox(height: 20,),
        Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(children: <Widget>[
                TextFormField(
                  onFieldSubmitted: (_){
                    FocusScope.of(context).requestFocus(_focusEmailNode);},
                  decoration: InputDecoration(
                    hintText: 'Name',
                  ),
                  validator: (value){
                    if(value.isEmpty || !value.contains("@")){
                      return "This Name Already Taken";
                    }return null;
                  },
                  onSaved: (value){},
                ),
                SizedBox(height: 30,),
                TextFormField(
                  focusNode: _focusEmailNode,
                  onFieldSubmitted: (_){
                    FocusScope.of(context).requestFocus(_focusPasswowdNode);},
                  decoration: InputDecoration(
                    hintText: 'E-Mail',
                  ),
                  validator: (value){
                    if(value.isEmpty || !value.contains("@")){
                      return "Invaild Email";
                    }return null;
                  },
                  onSaved: (value){},
                ),
                SizedBox(height: 30,),
                TextFormField(
                  obscureText: true,
                  focusNode: _focusPasswowdNode,
                  decoration: InputDecoration(
                      hintText: 'Password',
                      suffixIcon: Icon(Icons.remove_red_eye)
                  ),
                  validator: (value){
                    if(value.isEmpty || value.length < 5){
                      return "Your Password Must Be > 5";
                    }return null;
                  },
                  onSaved: (value){},

                )
              ],),
            )
        ),
        SizedBox(height: 25,),
        Padding(
          padding: const EdgeInsets.only(left: 30,right: 30),
          child: RaisedButton(
              shape: StadiumBorder(),
              color: Colors.black,
              child: Padding(
                padding: const EdgeInsets.only(left: 125,right: 125,top: 15,bottom: 15),
                child: Text('Sign Up',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white),),
              ),
              onPressed: sumbit),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Already have an account ?',style: TextStyle(color: Colors.grey),),
            FlatButton(
              child: Text('Log In',style: TextStyle(fontWeight: FontWeight.w400)),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>Login()));
              },
            )
          ],
        ),


      ],),
    );
  }
}

