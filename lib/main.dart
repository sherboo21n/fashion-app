import 'package:fashionshop/Screans/home_page.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'Providers/order_provider.dart';
import 'Providers/product_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Order()),
        ChangeNotifierProvider.value(value: Product()),
      ],
      child: MaterialApp(
          theme: ThemeData(
            primaryColor: Colors.blueAccent,
          ),
          debugShowCheckedModeBanner: false,
          debugShowMaterialGrid: false,
          home: HomePage()
      ),

    );
  }
}