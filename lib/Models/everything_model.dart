import 'package:flutter/cupertino.dart';

class everyThing{
  final String title;
  final String desc;
  final double price;
  final String image;

  everyThing({this.title,this.desc,this.image ,this.price,});
}

List<everyThing> products = [
  everyThing(
    title: 'Women T-shirt',
    desc:  'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. Conditions Apply!',
    price: 77.0,
    image: 'images/women_t-shirt.png'
  ),
  everyThing(
      title: 'Women Casual Pan',
      desc:  'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. Conditions Apply!',
      price: 58.0,
      image: 'images/women_2.png'
  ),
  everyThing(
      title: 'Women Short',
      desc:  'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. Conditions Apply!',
      price: 95.12,
      image: 'images/women_coll.png'
  ),
  everyThing(
      title: 'Women Pullover',
      desc:  'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. Conditions Apply!',
      price: 25.0,
      image: 'images/model.png'
  )
];