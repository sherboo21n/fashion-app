class category{
  final String title;

  category({this.title});
}

List<category> categories = [
  category(
    title: 'Everything'
  ),
  category(
    title: 'Jackets'
  ),
  category(
    title: 'Blazer'
  ),
  category(
    title: 'Dresses'
  ),
  category(
    title: 'Shoes'
  )
];