import 'package:flutter/material.dart';

class Size {
  final Container container2;
  Size({this.container2});
}

List<Size> ssss = [
  Size(
      container2: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey.shade300,
        ),
        child: Center(child: Text('S')),
      )
  ),

  Size(
      container2: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey.shade300,
        ),
        child: Center(child: Text('M')),
      )
  ),
  Size(
      container2: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey.shade300,
        ),
        child: Center(child: Text('L')),
      )
  ),
  Size(
      container2: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey.shade300,
        ),
        child: Center(child: Text('XL')),
      )
  )
];

