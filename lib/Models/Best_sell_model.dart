

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BestSellModel{
  final String image;
  final String title;
  final double prics;
  final String desc1;
  final String desc2;

  BestSellModel({this.image,this.title,this.prics,this.desc1,this.desc2});
}

List<BestSellModel> best = [
  BestSellModel(
    image: 'images/women_t-shirt.png',
    title: 'Women T-shirt',
    prics: 44.0,
      desc1: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI. ',
      desc2: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. '
          'Conditions Apply! FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI.'
  ),
  BestSellModel(
    image: 'images/men_t-shirt.png',
    title: 'Mink Spring Shirt',
    prics: 22.99,
    desc1: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
        'Applicable only on using Bank Of Baroda Credit Cards and EMI. ',
    desc2: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
        'Applicable only on using Bank Of Baroda Credit Cards and EMI transactions. '
        'Conditions Apply! FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
        'Applicable only on using Bank Of Baroda Credit Cards and EMI transactions.'
  ),
  BestSellModel(
    image: 'images/women_2.png',
    title: 'Women Pan',
    prics: 99.0,
      desc1: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI. ',
      desc2: 'FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI. '
          'Conditions Apply! FLAT 10% cashback on purchase of Grocery Essentials, Fashion Apparels, and Accessories from Paytm Mall. '
          'Applicable only on using Bank Of Baroda Credit Cards and EMI transactions.'
  ),
];

