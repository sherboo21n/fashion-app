import 'package:flutter/material.dart';

class colors {
  final Container container;
  colors({this.container});
}

List<colors> cccc = [
  colors(
      container: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.yellow,
        ),
      )
  ),

  colors(
      container: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.red,
        ),
      )
  ),
  colors(
      container: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey.shade300,
        ),
      )
  ),
  colors(
      container: Container(
        margin: EdgeInsets.symmetric(horizontal: 20 / 2.5),
        padding: EdgeInsets.all(3),
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.black,
        ),
      )
  )
];

