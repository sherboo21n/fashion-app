import 'package:fashionshop/Models/explore.dart_model';

List<ExploreModel> explore2 = [
  ExploreModel(
    image: 'images/women_coll.png',
    title: 'Designer collection',
    desc: 'Up To 50%',
  ),
  ExploreModel(
      image: 'images/designer_coll.png',
      title: 'Women collection',
      desc: 'Up To 30%'
  ),
  ExploreModel(
    image: 'images/men_coll.png',
    title: 'Men`s collection',
    desc: 'Up to 25%',
  ),
];