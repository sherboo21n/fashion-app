import 'package:fashionshop/Models/Best_sell_model.dart';
import 'package:flutter/material.dart';

class Order extends ChangeNotifier {

  List<BestSellModel> order = [];

  double _totalPrice= 0.0;
  double _totaldiscount = 0.0;
  int _totalQuant = 0;
  int qaunt = 1;
  DateTime _dateTime = DateTime.now() ;


  void add(BestSellModel bestSellModel) {
    order.add(bestSellModel);
    _totalPrice += bestSellModel.prics;
    _totalQuant += qaunt;
    notifyListeners();
  }

  void remove(BestSellModel bestSellModel) {
    order.remove(bestSellModel);
    _totalPrice -= bestSellModel.prics;
    _totalQuant -= qaunt;
    notifyListeners();
  }


  int get count {
    return order.length;
  }

  double get totalPrice {
    return _totalPrice;
  }

  double get totalDiscount {
    return _totaldiscount;
  }

  int get totalQuant {
    return _totalQuant;
  }

  DateTime get dataTime {
    return _dateTime;
  }
}