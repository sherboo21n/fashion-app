


import 'package:fashionshop/Models/Best_sell_model.dart';
import 'package:flutter/cupertino.dart';

class Product extends ChangeNotifier {

  List<BestSellModel> product = best;



  double _totalPrice= 0.0;
  int _totalQuant = 0;
  int qaunt = 1;
  DateTime _dateTime = DateTime.now() ;




  void add(BestSellModel bestSellModel) {
    product.add(bestSellModel);
    _totalPrice += bestSellModel.prics;
    _totalQuant += qaunt;
    notifyListeners();
  }

  void remove(BestSellModel bestSellModel) {
    product.remove(bestSellModel);
    _totalPrice -= bestSellModel.prics;
    _totalQuant -= qaunt;
    notifyListeners();
  }


  int get count {
    return product.length;
  }

  double get totalPrice {
    return _totalPrice;
  }

  int get totalQuant {
    return _totalQuant;
  }

  DateTime get dataTime {
    return _dateTime;
  }
}